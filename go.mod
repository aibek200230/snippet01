module se01.com

go 1.15

require (
	github.com/bmizerany/pat v0.0.0-20170815010413-6226ea591a40
	github.com/jackc/pgproto3/v2 v2.0.7 // indirect
	github.com/jackc/pgx/v4 v4.10.1
	github.com/justinas/alice v1.2.0
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
)
